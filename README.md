<img src="logo512.png" width="250">

## Introduction

The ***Knowl Bookshelf*** aims to be a unified front-end for all of your ebook databases.  Often, there may be significant structural differences between various ebook sources (i.e. Project Gutenberg Project, Internet Archive, Open Library, etc). ***Knowl Bookshelf*** mutates these differing sources into a common set of univeral Knowl fields. This consolidated/unified database is then presented to the end user as an elegant Single-Page Application (SPA). 

If you are looking for books (pdf, epub, etc) you are in the wrong place.  This page in only for the organization and retrieval of your existing book lists.  If you want to download some free books, I highly recommend you visit https://www.gutenberg.org/.  However, you do not need to own a single eBook to work through this project.

Specifically Knowl Bookshelf is a way to manage/organize all of your ebook metadata (Title, Author, ISBN, covers, hyperlinks, etc).  This makes Knowl Bookshelf suitable for small community libraries, developing regions, or anywhere that you wish to have a library search system on a limited budget.

Knowl Bookshelf search is functionally independent from whatever ebook files you reference.  This facilitates having Knowl Bookshelf on one or more cheaper resource constrained devices, while the ebook files themselves are on a remote (or local) file server.  If the remote ebook file server becomes temporarily unavailable, Knowl Bookshelf will still be fully functional (i.e. search/sort) albeit with temporarily broken hyperlinks.

For illustrative purposes, the LG metadata may be used to test Knowl Bookshelf.  However, you may wish to use Project Gutenburg data in a production environment if you are unsure of your local regulations.

## Objective

Setup a search engine for eBooks.  Search technology will be based on Elasticsearch, Logstash, and Kibana (collectively referred to as the “ELK Stack”).  We also have some advanced feautes planned on our <a href="https://gitlab.com/lucidhack/knowl/-/wikis/Roadmap-Features">***Features Roadmap***</a> page.    

We will be using LG book listings as our example data. However, with some modification, these techniques should also work for other publicly available book lists.  The reader is encouraged to expand these techniques to additional datasets, and share their logstash *.config files (submit a merge request for your *.config to the conf.d folder

## Screenshots

Detail View (default): 

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/250e4fe95657baff6c6006ba25c1347b/detail-view.png" width="800">

Bookshelf View:

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/e5a99b3d8c087759d609bca0ccc2ed02/grid-view.png" width="800">

## Procedural Book-Cover Generation

If a book-cover does not exist on disk, one will be generated on the fly. There are 256 different texture backgrounds, and various text and decoration styles.  Combined, this allows for a wide range of book-styles that will be automagically applied to books with no covers. A book-style (texture and label) is each determined at run-time by characters in the MD5 hash.  This ensures that the same book-style cover is applied across different systems.  

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/0641b1ad227c677377d17b99b4ad62e8/random-cover-generator.png" width="800">

## Components Overview

**Knowl-Bookshelf** is comprised of the Knowl-Interface, as well as several supporting software components. 
 For documentation purposes, "Knowl-Bookshelf" refers collectively to all of the different systems components as a whole (i.e. ELK, Mongo, Maria, React, Knowl-Interface, etc).  Whereas "Knowl-Interface" refers specifically to the React web application code found in <a href="https://gitlab.com/lucidhack/knowl/-/blob/dev/bookshelf/src/App.js">this file</a>.   

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/e2a99374b4ed7cf1536cd10cab106d4c/overview-diagram.png" width="800">

**React** is an open-source, front end, JavaScript library for building user interfaces or UI components.  React was selected for the Knowl-Interface, primarily because it easily supported ELK, and was easy enough for me to figure out considering that I am an intermittent hobby programmer at best.  The (react) Knowl-Interface is how most Knowl users will interact with the system.  This interface is optimized for simplicity, ease of use, and offers a great number of ways to search for data.  To learn more about the Knowl-Interface, visit the <a href="https://gitlab.com/lucidhack/knowl/-/wikis/User's-Guide">User's Guide</a>.

The Knowl-Interface uses **Elasticsearch** for its search capability.  Elasticsearch is a distributed, free and open search and analytics engine for all types of data, including textual, numerical, geospatial, structured, and unstructured.  The great support for unstructured data without compromising speed or flexibility made Elasticsearch a great fit for the Knowl-Bookshelf.  Elasticsearch is where we store all of our consolidated book libraries from multiple sources (i.e. project Gutenberg, LG, etc) with all of the field-names remapped to universal Knowl fields.  (see <a href="https://gitlab.com/lucidhack/knowl/-/wikis/References/Knowl-Fields">Knowl Fields</a> for more information.)

**Logstash** is a helper program that facilitates pulling in metadata from various external sources, and pushing that data into the Elasticsearch engine.  Logstash also performs the remapping of field names into the universal <a href="https://gitlab.com/lucidhack/knowl/-/wikis/References/Knowl-Fields">Knowl Fields</a>.  There is a corresponding logstash config file for each external book source.  Using the <a href="https://gitlab.com/lucidhack/knowl/-/tree/dev/conf.d">supplied examples</a>, you should be able to modify these to create connections to more/new book sources. 

The early/legacy version of Knowl Bookshelf used MariaDB as its input database (SQL to logstash).  However, JSON offers many improvements, so we now prefer MongoDB and/or OrbitDB for JSON to Logstash transactions.  There are some <a href="https://gitlab.com/lucidhack/knowl/-/tree/dev/legacy">legacy scripts</a> that have not yet been fully retired, that convert MariaDB SQL into flat JSON files.

**Kibana** is a powerful interface that lets you directly interact with Elasticsearch data.  Kibana is intended for data administration functions and/or power users (i.e. very advanced search or analytics).

Collectively, Elasticsearch Logstash and Kibana is known as the **"ELK" stack**.  More information on ELK can be found here: https://www.elastic.co/what-is/elk-stack

## Universal Knowl-Fields (Elasticsearch Field Mapping)

One goal of this project is to support many different/arbitrary book databases, and present them collectively in a unified front-end search application.  Each database will be stored intact and umodified in MariaDB.  When we perform the indexing of MariaDB into Elasticsearch, we will map each databases unique filed names into a coresponding *universal* Elasticsearch Knowl field.  This remapping of the source data into the unified Knowl fields will consolidate similar fields as well as making it easier for users to tailor the system to their own unique requirements (by redefining the mappings), without  modifying the underlying Maria databases. The re-mapping occurs within the `filter {}` section of the <a href="https://gitlab.com/lucidhack/knowl/-/tree/master/conf.d">Logstash configuration files</a>.

You can view the current/proposed ***Universal Knowl-Fields*** <a href="https://gitlab.com/lucidhack/knowl/-/wikis/References/Knowl-Fields">here</a>.  The book metadata is stored in Elasticsearch using these Knowl-Fields, and subsequently are the fields exposed within the Knowl-Interface.

## Getting Started

Proceed to the <a href="https://gitlab.com/lucidhack/knowl/-/wikis/Getting-Started"><b>Getting Started</b></a> Wiki page to get ***Knowl Bookshelf*** up and running on your own system.
