// REACT
import React, { Component, useContext, createContext, useState} from 'react'
import extend from 'lodash/extend'

import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";

// Collapsible section, used or "Table of Contents", and "Meta Data"
import Collapsible from 'react-collapsible';

// Elasticsearch functionality
// https://github.com/searchkit/searchkit
import { SearchkitManager,SearchkitProvider,
  SearchBox, Pagination, MenuFilter, TagCloud,
  HitsStats, SortingSelector, NoHits, BoolMustNot,
  ResetFilters, CheckboxItemList, SideBar,
  ViewSwitcherHits, ViewSwitcherToggle,
  InputFilter, GroupedSelectedFilters, PageSizeSelector,
  Layout, TopBar, LayoutBody, LayoutResults, Select,CheckboxFilter, TermQuery,
  ActionBar, ActionBarRow, RefinementListFilter } from 'searchkit'

// Style Sheet
import './index.css'

// Various Icons for the user interface
// https://github.com/wmira/react-icons-kit
import Icon from 'react-icons-kit';
import {questionCircle} from 'react-icons-kit/fa/questionCircle'
import {chainBroken} from 'react-icons-kit/fa/chainBroken'
import {arrowCircleODown} from 'react-icons-kit/fa/arrowCircleODown'
import {hashtag} from 'react-icons-kit/fa/hashtag'
import {calendar} from 'react-icons-kit/fa/calendar'
import {library} from 'react-icons-kit/icomoon/library'
import {barcode} from 'react-icons-kit/icomoon/barcode'
import {floppyDisk} from 'react-icons-kit/icomoon/floppyDisk'
import {book} from 'react-icons-kit/fa/book'
import {documents} from 'react-icons-kit/entypo/documents'
import {tags} from 'react-icons-kit/fa/tags'
import {user} from 'react-icons-kit/fa/user'
import {buildingO} from 'react-icons-kit/fa/buildingO'
import {language} from 'react-icons-kit/fa/language'
import {folderOpen} from 'react-icons-kit/fa/folderOpen'
import {caretRight} from 'react-icons-kit/fa/caretRight'
import {idBadge} from 'react-icons-kit/fa/idBadge'
import {paintBrush} from 'react-icons-kit/fa/paintBrush'
import {box} from 'react-icons-kit/feather/box'
import {fileText} from 'react-icons-kit/fa/fileText'
import {mapSigns} from 'react-icons-kit/fa/mapSigns'

// Global Variables
var cover=""
var coverstyle=""
var covertitle=""
var ipfsFilename = ""
var booklink=""
var linkstatus=""
var bookalt=""
var download = ""

{/* Function to check the existance of files */}
function UrlExists(path) {
  var http = new XMLHttpRequest();
  http.open('HEAD', path, false);
  http.send();
  return http.status!==404;
}

const AuthContext = React.createContext(false);

{/* Define our connection to the Elasticsearch instance, and the search pattern */}
const host = ("" + process.env.REACT_APP_ES_HOST + "" + process.env.REACT_APP_ES_ROUTE);
if(host == ""){
  console.log("You need to set your ES_HOST, DEFAULT_INDEX_ROUTE, and ES_SECURE environment variables!")
}
else {
  console.log("Connecting to host at " + host)
}
{/* VIEW: SIMPLE */}
{/* This view is displayed when the user selects "Simple" from the View menu in Knowl interface */}
{/* This view is a placeholder view, and is intented to eventually produce a machine readable format (ie CSV, etc). */}
{/* This view also serves to illustrate the basic functionality of a Knowl search results page */}
const ViewSimpleItem = (props)=> {
  const {result} = props
  const source = extend({}, result._source, result.highlight)
  return (
    <div className="simple-view">
      <pre><font size="1">id={source.id} Author={source.Author} Series={source.Series} Title={source.Title} Publisher={source.Publisher} Year={source.Year} Pages={source.Pages} Language={source.Language} Size={source.Size} Extension={source.Extension} ISBN={source.ISBN}</font></pre>
      <hr/>
    </div>
  )
}

{/* VIEW: DETAIL */}
{/* This is the default "Detail" view in Knowl, and used to display search results */}
const ViewDetailItem = (props)=> {
  const {result} = props
  const source = extend({}, result._source, result.highlight)

  {/* The books filename, as it appears on the local filesystem */}
  let Filename = "./books/" + result._source.Index + "/" + result._source.Subfolder + "/" + result._source.Filename

  { /* The books (presumed) original filename: [Title.Extension] */}
  let OriginalFilename=result._source.Title + "." + result._source.Extension

  {/* Define some search hyperlinks */}
  let SearchAuthor = "?Author=\"" + result._source.Author + "\"" // Search Hyperlink used when user clicks on Author
  let SearchTopic = "?Topic=\"" + result._source.Topic + "\""    // Search Hyperlink used when user clicks on Topic
  let SearchTitle = "?Title=\"" + result._source.Title + "\""    // Search Hyperlink used when user clicks on Title

  {/* IPFS DOWNLOAD LINK */}
  ipfsFilename=""
  if(source.Index==="Fiction"){
    {/* ipfsFilename = "http://localhost:8080/ipfs/" + result._source.IPFS_Blake2b */}
    ipfsFilename = "https://ipfs.io/ipfs/" + result._source.IPFS_Blake2b
  }
  if(source.Index==="Science"){
    {/* ipfsFilename = "http://localhost:8080/ipfs/" + result._source.IPFS_Blake2b */}
    ipfsFilename = "https://ipfs.io/ipfs/" + result._source.IPFS_Blake2b
  }

  {/* CHECK TO SEE IF THE EBOOK EXISTS */}
  booklink=Filename
  if(UrlExists(Filename)){
    {/* The book was found on LAN */}
    linkstatus=arrowCircleODown                        // Set the icon to a "Circled Down Arrow"
    bookalt=Filename                                   // Set hyperlink hover text to the books filename
  }else{
    {/* The books was NOT found on LAN */}
    linkstatus=chainBroken                             // Set the icon to a "Broken Chain Link"
    bookalt="Local book not found on LAN. Try IPFS..." // Set the hyperlink hover text to suggest IPFS
    booklink="javascript:void(0);"                     // Nullify the hyperlink
    Filename=""                                        // Nullify the Filename
    OriginalFilename=""                                //Nullify the OrginalFilename
  }

  {/* SET COVER TO WHERE WE EXPECT IT */}
  cover="/covers/" + result._source.Index + "/" + result._source.Cover

  {/* CHECK TO SEE IF BOOK-COVER EXISTS */}
  if(UrlExists(cover)&&result._source.Cover!=""){
    {/* The book-cover was found on disk, so we do not need to generate one */}
    coverstyle=""
    covertitle=""
  }else{
   {/* The book-cover was NOT found on disk, so we will create a generic one */}
    coverstyle = "text-style"
    covertitle=result._source.Title
    {/* Use two characters of the MD5 hash to select 1 of 256 available generic images from: */}
    {/* ~/knowl/bookshelf/public/coverless/skin/[00-FF].jpg */}
    cover="/coverless/skin/" + result._source.MD5.slice(-2).toUpperCase() + ".jpg"

    {/* ADD BLURRED BACKGROUND TO TEXT FOR BUSIER BACKGROUNDS */}
    coverstyle="text-style-noblur"
    if(parseInt(result._source.MD5.slice(-2).toUpperCase(),16)>128){
      coverstyle="text-style-blur"
    }
  }

  let prefix = '<br/><font color=#3b5998><b>'
  let suffix = '</b></font>'
  let showTopic    =    prefix + 'Topic: ' + suffix + result._source.Topic;
  let showTags =        prefix + 'Tags: ' + suffix + result._source.Tags;
  let showDescription = prefix + 'Description: ' + suffix + result._source.Description;
  let showTOC =         prefix + 'Table of Contents: ' + suffix + "<br/><br/>" + result._source.TOC;
  if (result._source.Topic==""||typeof result._source.Topic === 'undefined'){showTopic=""}
  if (result._source.Tags==""||typeof result._source.Tags === 'undefined'){showTags=""}
  if (result._source.Description==""||typeof result._source.Description === 'undefined'){showDescription=""}
  if (result._source.TOC==""||typeof result._source.TOC === 'undefined'){showTOC=""}

  {/* Render the "Detail" view */}
  return (

    <div className="simple-view">

      {/* Display a graphical book-cover (either from disk, or generated */}
      <div class="container">
        <img src={cover} class="simple-cover2"/>
        <div class={coverstyle}>{covertitle}</div>
        <div class="text-style-glow">{covertitle}</div>
        <div class="text-style">{covertitle}</div>
      </div>

      <div className="simple-tab">
        {/* Display the TITLE, AUTHOR, CATEGORY, and TAGS */}
        <hr/><span className="simple-icon" title="Title"><Icon size={16} icon={fileText}/></span> <span title="Click here to see similar Titles"><a href={SearchTitle}><font size="4"><b dangerouslySetInnerHTML={{__html:source.Title}}/></font></a></span>
        <br/><span className="simple-icon" title="Author"><Icon size={16} icon={user}/></span> <span title="Click here to see more works by this author"><a href={SearchAuthor}><b>{source.Author}</b></a></span>
        {/* <br/><br/><span className="simple-icon" title="Topic"><Icon size={16} icon={mapSigns}/></span> <span title="Click here to see more works with this topic"><a href={SearchTopic}><b>{source.Topic}</b></a></span> */}
        <div dangerouslySetInnerHTML={{__html: showTopic}}></div>
        <div dangerouslySetInnerHTML={{__html: showTags}}></div>
        <div dangerouslySetInnerHTML={{__html: showDescription}}></div>
        <div dangerouslySetInnerHTML={{__html: showTOC}}></div>
        {/* Display the (collapsed) META DATA */}
        <br/><Collapsible trigger="[+] Meta Data" ClassName="Collapsible">
          <br/><span className="simple-icon" title="ID"><Icon size={16} icon={idBadge}/> {source.ID}</span> &nbsp;&nbsp; <span className="simple-icon" title="Publisher"><Icon size={16} icon={buildingO}/> {source.Publisher}</span> &nbsp;&nbsp; <span className="simple-icon" title="Year"><Icon size={16} icon={calendar}/> {source.Year}</span> &nbsp;&nbsp; <span className="simple-icon" title="Language"><Icon size={16} icon={language}/> {source.Language}</span>
          <br/><span className="simple-icon" title="Index"><Icon size={16} icon={library}/> {source.Index}</span> &nbsp;&nbsp; <span className="simple-icon" title="Subfolder"><Icon size={16} icon={folderOpen}/> {source.Subfolder}</span> &nbsp;&nbsp; <span className="simple-icon" title="Filesize"><Icon size={16} icon={floppyDisk}/> {source.Filesize}</span> &nbsp;&nbsp; <span className="simple-icon" title="eBook Extension"><Icon size={16} icon={book}/> {source.Extension}</span> &nbsp;&nbsp; <span className="simple-icon" title="Pages"><Icon size={16} icon={documents}/> {source.Pages}</span> 
          <br/><span className="simple-icon" title="ISBN"><Icon size={16} icon={barcode}/> {source.ISBN}</span>
          <br/><span className="simple-icon" title="Cover"><Icon size={16} icon={paintBrush}/> {source.Cover}</span>
          <br/><span className="simple-icon" title="MD5 Hash"><Icon size={16} icon={hashtag}/> {source.MD5}</span>
          <br/><span className="simple-icon" title="Series"><Icon size={16} icon={caretRight}/> {source.Series}</span> &nbsp;&nbsp; <span className="simple-icon" title="Periodical"><Icon size={16} icon={caretRight}/> {source.Periodical}</span> &nbsp;&nbsp; <span className="simple-icon" title="Edition"><Icon size={16} icon={caretRight}/> {source.Edition}</span> &nbsp;&nbsp; <span className="simple-icon" title="Volume"><Icon size={16} icon={caretRight}/> {source.Volume}</span>
        </Collapsible>
        {/* Display the DOWNLOAD LINKS (LAN and IPFS) */}
        <br/><span className="simple-icon" title={bookalt}><Icon size={16} icon={linkstatus}/> <a download={OriginalFilename} href={booklink} title={bookalt}>Local Area Network (LAN)</a></span>
        <br/><span className="simple-icon" title="IPFS Download"><Icon size={16} icon={box}/> <a download={OriginalFilename} href={ipfsFilename} title={ipfsFilename}>InterPlanetary File System (IPFS)</a></span>
      </div>
   </div>
  )
}

class Login extends Component {
  constructor(props){
    super(props);
    this.state = ({
      user: "",
      password: ""
    })
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  validateForm() {
    return this.state.user.length > 0 && this.state.password.length > 0;
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.userHasAuthenticated(this.state.user, this.state.password);
  }
  render() {
    return (
      <div className="Login" style={{maxWidth: "256px", margin: "0 auto"}}>
        <img alt="Knowl Bookshelf" src={"./logo512.png"} width="256px"/>
        <form onSubmit={this.handleSubmit} >
          <FormGroup controlId="email" bsSize="large">
            <FormLabel>User</FormLabel>
            <FormControl
              autoFocus
              value={this.state.user}
              onChange={e => this.setState({user: e.target.value})}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <FormLabel>Password</FormLabel>
            <FormControl
              value={this.state.password}
              onChange={e => this.setState({password: e.target.value})}
              type="password"
            />
          </FormGroup>
          <Button block bsSize="large" disabled={!this.validateForm()} type="submit">
            Login
          </Button>
        </form>
      </div>
    );
  
  }
}

Login.contextType = AuthContext;

{/* VIEW: BOOKSHELF */}
{/* This view displays a grid of bookcovers, without any text */}
{/* Usefull on small displays (ie phone), or whenever user wants to compact coverview */}
const ViewBookshelfItem = (props)=> {
  const {bemBlocks, result} = props

  {/* The books filename, as it appears on the local filesystem */}
  let Filename = "./books/" + result._source.Index + "/" + result._source.Subfolder + "/" + result._source.Filename

  { /* The books (presumed) original filename: [Title.Extension] */}
  let OriginalFilename=result._source.Title + "." + result._source.Extension

  {/* CHECK TO SEE IF THE EBOOK EXISTS */}
  booklink=Filename
  if(UrlExists(Filename)){
    booklink=Filename // Set hyperlink filename to the LAN location
    bookalt=Filename  // Set hyperlink hover text to the books filename
  }else{
    {/* The books was NOT found on LAN, so use IPFS instead */}
    ipfsFilename=""
    if(result._source.Index==="Fiction"){
      {/* ipfsFilename = "http://localhost:8080/ipfs/" + result._source.IPFS_Blake2b */}
      ipfsFilename = "https://ipfs.io/ipfs/" + result._source.IPFS_Blake2b
    }
    if(result._source.Index==="Science"){
      {/* ipfsFilename = "http://localhost:8080/ipfs/" + result._source.IPFS_Blake2b */}
      ipfsFilename = "https://ipfs.io/ipfs/" + result._source.IPFS_Blake2b
    }
    booklink=ipfsFilename
    bookalt=ipfsFilename
  }

  {/* SET COVER TO WHERE WE EXPECT IT */}
  cover="/covers/" + result._source.Index + "/" + result._source.Cover

  {/* CHECK TO SEE IF BOOK-COVER EXISTS */}
  if(UrlExists(cover)&&result._source.Cover!=""){
    {/* The book-cover was found on disk, so we do not need to generate one */}
    coverstyle=""
    covertitle=""
  }else{
   {/* The book-cover was NOT found on disk, so we will create a generic one */}
    coverstyle = "text-style"
    covertitle=result._source.Title
    {/* Use two characters of the MD5 hash to select 1 of 256 available generic images from: */}
    {/* ~/knowl/bookshelf/public/coverless/skin/[00-FF].jpg */}
    cover="/coverless/skin/" + result._source.MD5.slice(-2).toUpperCase() + ".jpg"

    {/* ADD BLURRED BACKGROUND TO TEXT FOR BUSIER BACKGROUNDS */}
    coverstyle="text-style-noblur"
    if(parseInt(result._source.MD5.slice(-2).toUpperCase(),16)>128){
      coverstyle="text-style-blur"
    }
  }

  {/* Render the "Bookshelf" View */}
  return (

    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} class="gridview" data-qa="hit">
      <a href={booklink} download={OriginalFilename} title={bookalt}>
        <div class="container">
          <img src={cover} class="simple-cover2"/>
          <div class={coverstyle}>{covertitle}</div>
          <div class="text-style-glow">{covertitle}</div>
          <div class="text-style">{covertitle}</div>
        </div>
      </a>
      <br/><br/>
    </div>
  )
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function deleteCookie( cname ) {
  document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;" + "SameSite=Strict";
}

{/* This is the Main Knowl Application */}
{/* Here we define the page layout (TopBar, SideBar, etc), */}
{/* as well as place search controls/filters and other elemets on the page */}
class App extends Component {

  constructor(props){
    super(props);
    const authToken = getCookie("user") + ":" + getCookie("password");
    const isAuthed = (authToken != ":") || process.env.REACT_APP_SECURE=="false";
    this.state = {
      isAuthenticated: isAuthed,
      searchkit: new SearchkitManager(host, {
        basicAuth: authToken
      })
      
    }

    this.userHasAuthenticated = this.userHasAuthenticated.bind(this);
    this.logout = this.logout.bind(this);

  }

  userHasAuthenticated(user, pass){
    const authToken = user + ":" + pass;
    console.log(authToken);
    setCookie("user", user, 1);
    setCookie("password", pass, 1);
    this.setState(state => ({
      isAuthenticated: true,
      searchkit: new SearchkitManager(host, {
        basicAuth: authToken
      })
    }));
  }

  showSettings (event) {
    event.preventDefault();
  }

  logout(){
    console.log("Logging out!");
    deleteCookie("user");
    deleteCookie("password");
   
    this.setState({
      isAuthenticated: false
    });
  }

  render() {
    if(!this.state.isAuthenticated){
      return <Login userHasAuthenticated={this.userHasAuthenticated}/>
    }
    return (
    <AuthContext.Provider value={{ isAuthenticated: this.state.isAuthenticated, userHasAuthenticated: this.userHasAuthenticated, searchkit: this.state.searchkit}}>
    <SearchkitProvider searchkit={this.state.searchkit}>

      <Layout>

          <TopBar>
            <div className="my-logo"><a href="https://gitlab.com/lucidhack/knowl"><img alt="Knowl Bookshelf" src={"./navbar-logo.png"} width="188"/></a></div>
            <SearchBox
              prefixQueryFields={["Title^3", "Author^2", "Description", "Topic", "Tags", "TOC", "ISBN", "MD5"]}
              queryFields={["Title^3", "Author^2", "Description", "Topic", "Tags", "TOC", "ISBN", "MD5"]}
              placeholder="Title, Author, Topic, Tags, Description, TOC, ISBN, MD5"
              autofocus={true}
              searchOnChange={false}
              searchThrottleTime={2000}
              queryOptions={{analyzer:"standard"}}
             />
            <div>
              <Button onClick={this.logout}> 
                Logout 
              </Button> 
            </div>
            <div style={{ color: '#BBBBBB' }}>&nbsp;&nbsp;<a style={{ color: '#BBBBBB' }} href="https://gitlab.com/lucidhack/knowl/-/wikis/User's-Guide"><Icon size={40} icon={questionCircle}/></a></div>
          </TopBar>

        <LayoutBody>

          {/* Detailed search panel on the left side of the page */}
          <SideBar>
            <MenuFilter id="Indices" title="Indicies" field="Index.keyword" listComponent={CheckboxItemList} />
            {/* <CheckboxFilter id="Bookcover" title="" label="Bookcover" filter={BoolMustNot(TermQuery("Cover.keyword", ''))}/> */}
            <br/>
            <InputFilter id="Title" searchThrottleTime={2000} title="" placeholder="Title" searchOnChange={false} queryFields={["Title"]} />
            <InputFilter id="Author" searchThrottleTime={2000} title="" placeholder="Author" searchOnChange={false} queryFields={["Author"]} />
            <InputFilter id="Publisher" searchThrottleTime={2000} title="" placeholder="Publisher" searchOnChange={false} queryFields={["Publisher"]} />
            <InputFilter id="Topic" searchThrottleTime={2000} title="" placeholder="Topic" searchOnChange={false} queryFields={["Topic"]} />
            <InputFilter id="TagsDescription" searchThrottleTime={2000} title="" placeholder="Tags, TOC, Description" searchOnChange={false} queryFields={["Description","Tags", "TOC"]} />
            <InputFilter id="Identifier" searchThrottleTime={2000} title="" placeholder="id, DOI, ISBN, ISSN, UDC" searchOnChange={false} queryFields={["ID", "DOI","ISBN","ISSN","UDC"]} />
            <InputFilter id="Hash" searchThrottleTime={2000} title="" placeholder="AICH, CRC32, MD5, SHA1" searchOnChange={false} queryFields={["AICH","CRC32","MD5","SHA1"]} />
            <InputFilter id="Year" searchThrottleTime={2000} title="" placeholder="Year" searchOnChange={false} queryFields={["Year"]} />
            <RefinementListFilter id="ProlificYears" title="Year" field="Year.keyword" operator="OR" size={6}/>
            <RefinementListFilter id="ProlificLanguage" title="Language" field="Language.keyword" operator="OR" size={6}/>
            <RefinementListFilter id="ProlificExtension" title="Extension" field="Extension.keyword" operator="OR" size={6}/>
            <RefinementListFilter id="ProlificTopic" title="Topic" field="Topic.keyword" operator="OR" size={6}/>
        </SideBar>


          <LayoutResults>
            <ActionBar>

              {/* Action Bar near top of screen */}
              <ActionBarRow>
                Per Page:<PageSizeSelector options={[1,24,48]} listComponent={Select}/>&nbsp;&nbsp;&nbsp;&nbsp;
                View:<ViewSwitcherToggle listComponent={Select}/>&nbsp;&nbsp;&nbsp;&nbsp;
                Sort:<SortingSelector options={[
                  {label:"Relevance", field:"_score", order:"desc"},
                  {label:"ID +", field:"ID", order:"asc"},
                  {label:"ID -", field:"ID", order:"desc"},
                  {label:"Title +", field:"Title.keyword", order:"asc"},
                  {label:"Title -", field:"Title.keyword", order:"desc"},
                  {label:"Author +", field:"Author.keyword", order:"asc"},
                  {label:"Author -", field:"Author.keyword", order:"desc"},
                  {label:"Publisher +", field:"Publisher.keyword", order:"asc"},
                  {label:"Publisher -", field:"Publisher.keyword", order:"desc"},
                  {label:"Year +", field:"Year", order:"asc"},
                  {label:"Year -", field:"Year", order:"desc"},
                  {label:"Pages +", field:"Pages", order:"asc"},
                  {label:"Pages -", field:"Pages", order:"desc"},
                  {label:"Language +", field:"Language.keyword", order:"asc"},
                  {label:"Language -", field:"Language.keyword", order:"desc"},
                  {label:"Size +", field:"Size", order:"asc"},
                  {label:"Size -", field:"Size", order:"desc"},
                  {label:"Extension +", field:"Extension.keyword", order:"asc"},
                  {label:"Extension -", field:"Extension.keyword", order:"desc"},
                  {label:"psuedo-random", field:"MD5.keyword", order:"asc"},
                  {label:"Cover +", field:"Cover.keyword", order:"asc"},
                  {label:"Cover -", field:"Cover.keyword", order:"desc"},
                ]}/>&nbsp;&nbsp;&nbsp;&nbsp;
                <b><HitsStats translations={{"hits6tats.results_found":"{hitCount} Results Found"}}/></b>

              </ActionBarRow>

              <ActionBarRow>
                <GroupedSelectedFilters/>
                <ResetFilters/>
              </ActionBarRow>

            </ActionBar>

            <ViewSwitcherHits
                hitsPerPage={24}
                highlightFields={["Title", "Tags", "Description"]}
                sourceFilter={["IPFS", "TOC", "Index", "Library", "ID", "zero", "Author", "Visible", "Description", "IPFS_Blake2b", "Title", "Subfolder", "Cover", "ISBN", "MD5", "AICH", "SHA1", "CRC32", "Year", "Language", "Publisher", "Extension","Topic","Tags","DOI","ISSN","UDC","Filename","Extension","Filesize","Pages"]}
                hitComponents={[
                  {key:"grid", title:"Bookshelf", itemComponent:ViewBookshelfItem},
                  {key:"table", title:"Simple", itemComponent:ViewSimpleItem},
                  {key:"list", title:"Detail", itemComponent:ViewDetailItem, defaultOption:true}
                ]}
                scrollTo="body"
            />
            <NoHits suggestionsField={"Title"}/>
            <Pagination showNumbers={true}/>

          </LayoutResults>
          </LayoutBody>
        </Layout>
      </SearchkitProvider>
    </AuthContext.Provider>


    );
  }
}

export default App;
