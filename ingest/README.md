# Ingest Folder

The ingest folder is where the scripts will temporarily dowload databases and book-covers to.  This working/staging directory is where the databases will then be unpacked and then loaded into MariaDB.
