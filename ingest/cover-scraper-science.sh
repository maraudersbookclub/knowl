#!/bin/bash

# Download the first 100 bookcovers from the libgen-science collection
# Modify or delete "AND id < 100" to get more records,
# or change to "AND id > 1000000" to start at a last known point (ie 1M)

sudo chmod -Rv 777 ../bookshelf/public/covers

echo "Consolidating covers from last run..."
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.rs/covers/ ../bookshelf/public/covers/Science/
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.lc/covers/ ../bookshelf/public/covers/Science/
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.li/covers/ ../bookshelf/public/covers/Science/
#sudo rm -r ../bookshelf/public/covers/Science/libgen.rs
#sudo rm -r ../bookshelf/public/covers/Science/libgen.lc
#sudo rm -r ../bookshelf/public/covers/Science/libgen.li

echo "Downloading new covers (using libgen.updated)..."
sudo mysql -e "SELECT coverurl FROM libgen.updated WHERE coverurl != '' AND id < 100 ORDER BY id" | while read coverurl; do
  # Check local disk for book cover
  if [ ! -f ../bookshelf/public/covers/Science/$coverurl ]; then
    echo "/n=============================================================="
    echo "MISSING ON DISC: "$coverurl
    if [[  $coverurl == "http"* ]]; then
      echo "  1. Trying STANDARD http(s):// url"
      wget -P ../bookshelf/public/covers/Science/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories $coverurl
    elif [[  $coverurl == "/images/I/"* ]]; then
      echo "  1. Trying AMAZON url"
      wget -P ../bookshelf/public/covers/Science/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories "http://ecx.images-amazon.com"$coverurl
    else
      echo "  1. Trying PRIMARY .rs mirror (truncated LG url)..."
      if ! wget -P ../bookshelf/public/covers/Science/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories "http://libgen.rs/covers/"$coverurl; then
        echo "  2. Trying SECONDARY .lc mirror (truncated LG url)"
        if ! wget -P ../bookshelf/public/covers/Science/ -c --force-directories "https://libgen.lc/covers/"$coverurl; then
          echo "3. Trying TERTIARY .li mirror (truncated LG url)"
            wget -P ../bookshelf/public/covers/Science/ -c --force-directories "https://libgen.li/covers/"$coverurl
        fi
      fi
    fi
  else
    echo "FOUND ON DISK (skipping download); "$coverurl
  fi
done

echo "Consolidating covers from this run..."
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.rs/covers/ ../bookshelf/public/covers/Science/
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.lc/covers/ ../bookshelf/public/covers/Science/
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.li/covers/ ../bookshelf/public/covers/Science/
#sudo rm -r ../bookshelf/public/covers/Science/libgen.*
#sudo rm -r ../bookshelf/public/covers/Science/libgen.lc
#sudo rm -r ../bookshelf/public/covers/Science/libgen.li

echo "Downloading new covers (using libgen.updated_edited)..."
sudo mysql -e "SELECT coverurl FROM libgen.updated_edited WHERE coverurl != '' AND id < 100 ORDER BY id" | while read coverurl; do
  # Check local disk for book cover
  if [ ! -f ../bookshelf/public/covers/Science/$coverurl ]; then
    echo "/n=============================================================="
    echo "MISSING ON DISC: "$coverurl
    if [[  $coverurl == "http"* ]]; then
      echo "  1. Trying STANDARD http(s):// url"
      wget -P ../bookshelf/public/covers/Science/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories $coverurl
    elif [[  $coverurl == "/images/I/"* ]]; then
      echo "  1. Trying AMAZON url"
      wget -P ../bookshelf/public/covers/Science/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories "http://ecx.images-amazon.com"$coverurl
    else
      echo "  1. Trying PRIMARY .rs mirror (truncated LG url)..."
      if ! wget -P ../bookshelf/public/covers/Science/ --tries 2 --timeout 5 -c --no-check-certificate --force-directories "http://libgen.rs/covers/"$coverurl; then
        echo "  2. Trying SECONDARY .lc mirror (truncated LG url)"
        if ! wget -P ../bookshelf/public/covers/Science/ -c --force-directories "https://libgen.lc/covers/"$coverurl; then
          echo "3. Trying TERTIARY .li mirror (truncated LG url)"
            wget -P ../bookshelf/public/covers/Science/ -c --force-directories "https://libgen.li/covers/"$coverurl
        fi
      fi
    fi
  else
    echo "FOUND ON DISK (skipping download); "$coverurl
  fi
done

echo "Consolidating covers from this run..."
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.rs/covers/ ../bookshelf/public/covers/Science/
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.lc/covers/ ../bookshelf/public/covers/Science/
sudo rsync -a -v --stats --progress --remove-source-files ../bookshelf/public/covers/Science/libgen.li/covers/ ../bookshelf/public/covers/Science/
#sudo rm -r ../bookshelf/public/covers/Science/libgen.*
#sudo rm -r ../bookshelf/public/covers/Science/libgen.lc
#sudo rm -r ../bookshelf/public/covers/Science/libgen.li
