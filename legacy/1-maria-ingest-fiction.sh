#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Fiction                         ##
## Filename: ./legacy/1-maria-ingest-fiction.sh             ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f fiction.rar

# Fetch the fiction database from your favorite mirror
wget -c http://libgen.rs/dbdumps/fiction.rar

# Unpack the database
unrar x fiction.rar
rm -f fiction.rar

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' fiction.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen_fiction;"
sudo mysql -e "CREATE DATABASE libgen_fiction;"

# Import our databse into MariaDB
sudo mysql libgen_fiction < fiction.sql
rm -f fiction.sql

# Next, you should call the json script
# sudo sh ../legacy/2-maria-to-json-fiction.sh
