#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST fiction                         ##
## Filename: ./legacy/2-maria-to-json-fiction.sh            ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# PURGE EXISTING FILES
cd /var/tmp
sudo rm -f ~/knowl/ingest/knowl-libgen-fiction.json
sudo rm -f /var/tmp/knowl-libgen-fiction*.json

# GENERATE MANY JSON FILES (100K at a time)
counter=0
while [ $counter -le 3000000 ]
do
echo $counter
sudo mysql <<ENDSQL
use libgen_fiction;
DROP TABLE IF EXISTS knowl;
SET connect_work_size = 4000000000;
create table knowl (Description char(255)) engine=CONNECT table_type=JSON file_name='/var/tmp/knowl-libgen-fiction-$counter.json'
option_list='Pretty=1,Jmode=0,Base=1' lrecl=128 as
SELECT fiction.ID,
       fiction.Title,
       fiction.Author,
       fiction.Series,
       fiction.Edition,
       fiction.Language,
       fiction.Year,
       fiction.Publisher,
       fiction.Pages,
       fiction.Identifier,
       fiction.ASIN,
       fiction.Coverurl "Cover",
       fiction.Extension,
       fiction.Filesize,
       fiction.Library,
       fiction.Issue,
       fiction_hashes.md5 "MD5",
       fiction_hashes.crc32 "CRC32",
       fiction_hashes.aich "AICH",
       fiction_hashes.sha1 "SHA1",
       fiction_hashes.tth "TTH",
       fiction_hashes.btih "BTIH",
       fiction_hashes.sha256 "SHA256",
       fiction_description.descr "Description"
       FROM libgen_fiction.fiction LEFT JOIN libgen_fiction.fiction_hashes ON fiction.md5=fiction_hashes.md5 LEFT JOIN libgen_fiction.fiction_description ON fiction.md5=fiction_description.md5 ORDER BY fiction.ID LIMIT $counter,250000;
DROP TABLE IF EXISTS knowl;
ENDSQL
counter=$(( $counter + 100000 ))
done

# COMBINE ALL OF THE 1K JSON FILES
sudo cat knowl-libgen-fiction*.json >> ~/knowl/ingest/knowl-libgen-fiction.json
sudo rm -f knowl-libgen-fiction*.json

# DELETE LINES CONTAINING ONLY "[" or "]",
# AND DELETE LEADING WHITESPACE FROM ALL LINES
cd ~/knowl/ingest
sudo sed -i '/^\[/d;/^\]/d;s/^[ \t]*//' knowl-libgen-fiction.json

# Next, you should call the index json script
# sudo sh ../bare-metal/index-fiction-json.sh
